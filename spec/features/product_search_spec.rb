require 'rails_helper'

RSpec.feature 'Product search', type: :feature do
  scenario 'Fill in search field, click search button will return search results', js: true do
    VCR.use_cassette('integration_product_search') do
      visit root_path
      fill_in 'search', with: 'asus'

      click_on 'Search'
      expect(page).to have_text('Asus Zenfone')

      click_on 'Asus Zenfone 5 A500CG'

      expect(page).to have_text('Launch')
      expect(page).to have_text('Status')
      expect(page).to have_text('Available. Released 2014, April')
    end
  end

  scenario 'Search by wrong query will show not found message', js: true do
    VCR.use_cassette('integration_not_found_search') do
      visit root_path
      fill_in 'search', with: 'gakdhgk;adhg;adhg;oaiehgo;ihq'

      click_on 'Search'
      expect(page).to have_text('not found')
    end
  end
end
