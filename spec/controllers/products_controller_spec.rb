require 'rails_helper'

RSpec.describe ProductsController, type: :controller do

  describe 'GET #index' do
    context 'nil query' do
      it 'returns http success' do
        VCR.use_cassette('products_index_nil_query') { get :index, xhr: true }

        expect(response).to have_http_status(:success)
      end
    end
    context 'real query' do
      it 'returns http success' do
        VCR.use_cassette('products_index_asus_query') { get :index, xhr: true, params: { query: 'asus' } }

        expect(response).to have_http_status(:success)
      end
    end
    context 'wrong query' do
      it 'returns http success' do
        VCR.use_cassette('products_index_wrong_query') { get :index, xhr: true, params: { query: 'wrong query' } }

        expect(response).to have_http_status(:success)
      end
    end
  end

  describe 'GET #show' do
    context 'correct id' do
      it 'returns http success' do
        VCR.use_cassette('products_show_gsm_arena_iphone_7') do
          get :show, xhr: true, params: { id: 'gsm_arena_apple_iphone_7_plus-8065' }
        end
        expect(response).to have_http_status(:success)
      end
    end

    context 'wrong id' do
      it 'returns http not found' do
        get :show, xhr: true, params: { id: 'wrong' }
        expect(response).to have_http_status(:not_found)
      end

    end

  end

end
