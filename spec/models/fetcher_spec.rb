require 'rails_helper'

RSpec.describe Fetcher, type: :model do
  describe '#get' do
    let(:path) { 'gsm_arena_apple_iphone_7_plus-8065.php' }

    context 'get real path' do
      it 'returns product' do
        allow_any_instance_of(Caller::GSMArena).to receive(:get) { Product.new(name: 'Apple iPhone 7 Plus') }

        result = subject.get(path)

        expect(result).to be_a Product
        expect(result.name).to eq 'Apple iPhone 7 Plus'
      end
    end

    context 'get wrong path' do
      it 'returns nil for path with correct prefix' do
        allow_any_instance_of(Caller::GSMArena).to receive(:get) { nil }

        result = subject.get('gsm_arena_wrong-path')

        expect(result).to be_nil
      end

      it 'returns nil for path without correct prefix' do
        expect_any_instance_of(Caller::GSMArena).not_to receive(:get)

        result = subject.get('wrong-path')

        expect(result).to be_nil
      end
    end

  end

  describe '#search' do
    context 'get real query' do
      it 'returns products' do
        allow_any_instance_of(Caller::GSMArena).to receive(:search) { [Product.new(name: 'Apple iPhone 7 Plus')] }

        result = subject.search('apple')

        expect(result).to be_an Array
        expect(result.first).to be_a Product
        expect(result.first.name).to eq 'Apple iPhone 7 Plus'
      end
    end

    context 'get wrong query' do
      it 'returns empty array' do
        allow_any_instance_of(Caller::GSMArena).to receive(:search) { [] }

        result = subject.search('wrong query')

        expect(result).to eq []
      end
    end
  end

end
