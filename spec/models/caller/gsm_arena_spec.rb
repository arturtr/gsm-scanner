require 'rails_helper'

RSpec.describe Caller::GSMArena, type: :model do
  describe '#get' do
    let(:path) { 'apple_iphone_7_plus-8065.php' }
    let(:endpoint) { "http://www.gsmarena.com/#{path}" }

    it 'returns product with proper data' do
      result = VCR.use_cassette('gsm_arena_iphone_7') { subject.get(path) }

      expect(result).to be_a Product
      expect(result.name).to eq 'Apple iPhone 7 Plus'
    end

    it 'returns nil with external failure' do
      stub_request(:get, endpoint).to_return(status: 503, body: nil)

      expect(subject.get(path)).to be_nil
    end
  end


  describe '#search' do
    context 'real query' do
      it 'returns products array with proper data' do
        result = VCR.use_cassette('gsm_arena_asus') { subject.search('asus') }

        expect(result).to be_an Array

        item = result.first

        expect(item).to be_a Product
        expect(item.name).to match(/asus/i)
      end
    end

    context 'wrong query' do
      it 'returns empty products array for wrong query' do
        result = VCR.use_cassette('gsm_arena_wrong_query') { subject.search('wrong_query') }

        expect(result).to eq []
      end
    end
  end

  describe '#normalize' do
    it 'remove gsm_arena_ prefix from path and add .php to end' do
      normalized_path = subject.send(:normalize, 'gsm_arena_phone_x21')
      expect(normalized_path).to eq 'phone_x21.php'
    end

    it 'not add php if it exists' do
      normalized_path = subject.send(:normalize, 'test.php')
      expect(normalized_path).to eq 'test.php'
    end
  end

  describe '#search_uri' do
    it 'returns search URI for query' do
      expect(subject.send(:search_uri, 'asus')).to eq URI.parse('http://www.gsmarena.com/results.php3?sQuickSearch=yes&sName=asus')
    end
  end
end
