require 'rails_helper'

RSpec.describe Builder::GSMArena, type: :model do
  describe '#build_item' do
    it 'build product from valid parsed HTML' do
      document = document_from_cassete('gsm_arena_iphone_7')

      result = subject.build_item(document)

      expect(result).to be_a Product
      expect(result.name).to eq 'Apple iPhone 7 Plus'
    end
  end

  describe '#build_list' do
    it 'build products array from valid parsed HTML' do
      document = document_from_cassete('gsm_arena_asus')

      result = subject.build_list(document)

      expect(result).to be_an Array

      item = result.first

      expect(item).to be_a Product
      expect(item.name).to match(/asus/i)
    end
  end

  private

  def document_from_cassete(name)
    cassete = VCR.insert_cassette(name)

    response = cassete.http_interactions.interactions.first.response

    VCR.eject_cassette(name)

    Oga.parse_html(response.body)
  end


end

