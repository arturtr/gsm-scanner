# GSM Scanner

Simple app which collect GSM phones data form different data sources:

* [GSM Arena](http://www.gsmarena.com/)
* other sources not able for now

# Installation

* clone repo
* go to repo directory
* `bundle install`
* `bin/rails s`

# Test running

`bin/rspec spec`
