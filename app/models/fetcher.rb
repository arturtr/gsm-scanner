class Fetcher

  attr_reader :callers

  def initialize(callers: [Caller::GSMArena.new])
    @callers = callers
  end

  def get(path)
    caller = caller_for(path) || Caller::Null.new
    caller.get(path)
  end

  def search(query)
    callers.map { |caller| caller.search(query) }.flatten
  end

  private

  def caller_for(path)
    if path.start_with?('gsm_arena_')
      callers.find{|c| c.is_a?(Caller::GSMArena)}
    end
  end
end
