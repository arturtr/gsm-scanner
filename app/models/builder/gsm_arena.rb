class Builder::GSMArena

  def build_item(document)
    name = document.at_css('h1').text

    image_url = document.at_css('.specs-photo-main img')&.get('src')

    specs = document.css('#specs-list table').map do |spec|
      spec_name = spec.at_css('th').text

      categories = spec.css('tr').map do |sc|
        OpenStruct.new(name: sc.at_css('.ttl')&.text, values: sc.at_css('.nfo')&.text)
      end

      OpenStruct.new(name: spec_name, categories: categories)
    end

    Product.new(name: name, image_url: image_url, specs: specs)
  end

  def build_list(document)
    document.css('.makers ul li a').map do |phone|
      name = phone.at_css('strong span').children.map(&:text).reject(&:empty?).join(' ')

      link = 'gsm_arena_' + phone.get('href')

      image_url = phone.at_css('img').get('src')

      Product.new(name: name, image_url: image_url, link: link)
    end
  end
end

