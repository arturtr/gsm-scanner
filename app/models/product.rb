class Product
  attr_reader :name, :image_url, :specs, :link

  def initialize(name:, image_url: '', link: nil, specs: [])
    @name      = name
    @image_url = image_url
    @link      = link
    @specs     = specs
  end

end
