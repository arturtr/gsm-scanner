class Caller::GSMArena < Caller

  URL = 'http://www.gsmarena.com/'

  private

  def builder
    Builder::GSMArena.new
  end

  def normalize(path)
    normalized_path = path.end_with?('.php') ? path : "#{path}.php"
    normalized_path.gsub('gsm_arena_', '')
  end

  def item_uri(path)
    URI.parse(URL + normalize(path))
  end

  def search_uri(query)
    URI.parse(URL + 'results.php3?sQuickSearch=yes&sName=' + query.to_s)
  end

end
