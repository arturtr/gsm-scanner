class Caller::Null

  def get(path)
    nil
  end

  def search(query)
    []
  end
end
