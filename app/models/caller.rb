class Caller

  def get(path)
    response = client.get_response(item_uri(path))
    return if response.code.to_i > 400

    document = parser.parse_html(response.body)
    builder.build_item(document)
  end

  def search(query)
    response = client.get_response(search_uri(query))
    document = parser.parse_html(response.body)

    builder.build_list(document)
  end

  private

  def client
    Net::HTTP
  end

  def parser
    Oga
  end

end
