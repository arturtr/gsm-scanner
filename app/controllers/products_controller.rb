class ProductsController < ApplicationController

  def index
    @query = params[:search]
    @products = fetcher.search(@query)
  end

  def show
    @product = fetcher.get(params[:id])
    @product || head(:not_found)
  end

  private

  def fetcher
    Fetcher.new
  end
end
